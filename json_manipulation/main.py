import json
import inspect
from datetime import datetime

class JsonManipulation:
    def __init__(self) -> None:
        with open("data.json") as data:
            self.data = json.load(data)
        
    def items_in_meeting_room(self):
        items = []
        for x in self.data:
            x["placement"]["name"] == "Meeting Room" and items.append(x)
        return items

    def electronic_devices(self):
        items = []
        for x in self.data:
            x["type"] == "electronic" and items.append(x)
        return items
    
    def furniture_type(self):
        items = []
        for x in self.data:
            x["type"] == "furniture" and items.append(x)
        return items

    def date_is_16_jan_2020(self):
        items = []
        for x in self.data:
            day = datetime.fromtimestamp(x["purchased_at"]).strftime('%d-%m-%y')
            day == "16-01-20" and items.append(x)
        return items
    
    def is_brown_color(self):
        items = []
        for x in self.data:
            "brown" in x["tags"] and items.append(x)
        return items

def asking_input():
    number = input("Input your selection : ")
    # Integer check
    script_executor(int(number)) if number.isdigit() else print("Please input only integer")

def script_executor(number):
    if number < len(class_body):
        ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[(n//10%10!=1)*(n%10<4)*n%10::4])
        data = class_body[number][1]() 
        for (i, item) in enumerate(data):
            print(f'{ordinal(i+1)} item')
            print(f'Name : {item["name"]}')
            print(f'Type : {item["type"]}')
            print(f'Tags : ', ', '.join(x for x in item["tags"]))
            print(f'Purchased at : {datetime.fromtimestamp(item["purchased_at"])}')
            print(f'Placement : {item["placement"]["name"]} \n')
    else:
        print("The number is out of reach")

def show_menu():
    count = 1
    print('Select method to be executed first : \n')
    for (item,fullpath) in class_body[1:]:
        print(f'{count}. {item}')
        count += 1
        
def continuation():
    show_menu()
    asking_input()
    while True:
        if input('\nWant to execute another script?(Y/n)').lower() == "y":
            show_menu()
            asking_input()
        else:
            print("Good Bye!")
            break


if __name__ == "__main__":
    manipulator = JsonManipulation()
    class_body = inspect.getmembers(manipulator, predicate=inspect.ismethod)
    continuation()