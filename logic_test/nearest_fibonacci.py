def asking_input():
    many = input("How many number?")
    many = int(many) + 1 if many.isdigit() else print("Please input only integer")

    ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[(n//10%10!=1)*(n%10<4)*n%10::4])

    input_array = []
    i = 1
    while i < many:
        for output in range(1, many):
            print(f'{output}. {input_array[output-1] if len(input_array) >= output else ""}')

        temp_input = input(f'The {ordinal(i)} number :  ')
        if temp_input.isdigit():
            temp_input = int(temp_input)  
            input_array.append(temp_input)
        else:
            print("Please input only integer")
            i -= 1
        i += 1
    
    get_nearest_fibonacci(adding_up_array(input_array))
    

def get_nearest_fibonacci(input_val):
    # Initialize fibonacci initial value
    fibo_num = [0,1]

    # Get fibonacci number until the the number is larger than input
    while fibo_num[-1] <= input_val:
        fibo_num.append(fibo_num[-1] + fibo_num[-2])

    # Get the nearest number
    nearest_fibo = min(fibo_num, key=lambda x:abs(x-input_val))
    print(f'The nearest Fibonacci Number is {nearest_fibo}')

    # Get the distance between input and the nearest fibonacci number
    print(f'Output : {abs(nearest_fibo - input_val)}')

def adding_up_array(arr):
    val = 0
    for i in arr: val += i
    print(f'Your number is {val}')
    return val  

def sample():
    input_array = [15, 1, 3]
    print("Print out the nearest fibonacci from combined number in an array")
    print("Sample with array :", input_array)
    input_val = adding_up_array(input_array)
    get_nearest_fibonacci(input_val)

def continuation():
    sample()
    while True:
        if input('\nWant to check another number?(Y/n)').lower() == "y":
            asking_input()
        else:
            print("Good Bye!")
            break

if __name__ == "__main__":
    continuation()