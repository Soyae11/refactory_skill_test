def print_out_leap_years(start_year, end_year):
    
    # Initialize array to store the value from recursive
    leaps = []
    while start_year <= end_year:
        start_year += 1

        # Looking for leap years only
        if start_year % 4 == 0:

            # Append the number to the array
            leaps.append(start_year)

    # Print out each value in the array
    print(*leaps)

def asking_input():
    start_year = input("Start year : ")
    end_year = input("Last year : ")
    exceptor(start_year, end_year)

def exceptor(start_year, end_year):
    if(start_year >= end_year):
        print("Cannot find the leap years. Please input start year less than the last year")
        return
    try:
        start_year = int(start_year)
        end_year = int(end_year)
    except ValueError:
        print("Cannot find the leap years. Please input the years with number")
        return

    print_out_leap_years(start_year, end_year)
    
def continuation():
    print("Find leap years in between two years")
    asking_input()
    while True:
        if input('\nWant to check another leap years?(Y/n)').lower() == "y":
            asking_input()
        else:
            print("Good Bye!")
            break

if __name__ == "__main__":
    continuation()