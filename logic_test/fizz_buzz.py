def asking_input():
    number = input("Input your integer : ")
    # Integer check
    array_constuctor(int(number)) if number.isdigit() else print("Please input only integer")

def array_constuctor(number):
    # Write out the condition
    pisbus_list = [
    (3,"Fizz"),
    (5,"Buzz")
    ]
    # Initialize array for storing the result
    val_array = []
    # Do recursive with 1 as starter
    for a in range(1,(number + 1)):
        fizzer = ""
        # Checking if the index is the same as condition
        for pbl in pisbus_list:
            if a % pbl[0] == 0:
                fizzer += pbl[1]
        # Store the result
        val_array.append(a) if fizzer == "" else val_array.append(fizzer)
    # Print out each value in array
    print(*val_array)

def continuation():
    print('Given an integer n, return a string array answer (1-indexed) where :')
    print(' - answer[i] == "FizzBuzz" // if i is divisible by 3 and 5.')
    print(' - answer[i] == "Fizz" // if i is divisible by 3.')
    print(' - answer[i] == "Buzz" // if i is divisible by 5.')
    print(' - answer[i] == i // if non of the above conditions are true.')
    asking_input()
    while True:
        if input('\nWant to check another number?(Y/n)').lower() == "y":
            asking_input()
        else:
            print("Good Bye!")
            break


if __name__ == "__main__":
    continuation()
