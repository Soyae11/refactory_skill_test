def asking_input():
    sentence = input("Input your sentence : ")
    reverser(sentence)

def reverser(sentence):
    
    # Split every words and put it into list
    words = sentence.split(' ')

    # Initialize another array to store the value from recursive
    reversed_words = []
    for word in words:

        # Store reversed word into the array that has been prepared
        reversed_words.append(word[::-1])

    # Print each word 
    print(*reversed_words)

def continuation():
    print("Print out the sentence with reversed word")
    asking_input()
    while True:
        if input('\nWant to check another sentence?(Y/n)').lower() == "y":
            asking_input()
        else:
            print("Good Bye!")
            break

if __name__ == "__main__":
    continuation()