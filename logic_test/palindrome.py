def polindrome_check(sentence): 
    # make string into lowercase and remove the trailing new line. Just in case there is trouble to recognize the reversed sentence
    lowercased_sentence = sentence.lower().rstrip('\n')

    # reverse sentence and compare to the original sentence
    return lowercased_sentence == lowercased_sentence[::-1]

def print_out_statement(x):
    print(x.rstrip('\n'), f'{"is" if polindrome_check(x) else "is not"} polindrome')

def print_out_sample():
    f = open("sample/palindrome.txt", "r")

    for x in f:
        # print the original sentence and the statement
        print_out_statement(x)
        
def continuation():
    print("Polindrome checking. Sample :\n")
    print_out_sample()
    while True: 
        if input('\nWant to check another sentence?(Y/n)').lower() == "y":
            x = input("Give me the sentence : ")
            print(x.rstrip('\n'), f'{"is" if polindrome_check(x) else "is not"} polindrome')
        else:
            print("Good Bye!")
            break

if __name__ == "__main__":
    continuation()