import palindrome
import leap_years
import reverse_word
import nearest_fibonacci
import fizz_buzz

# Dictionary of the script
script_list = {
    1:palindrome, 
    2:leap_years, 
    3:reverse_word, 
    4:nearest_fibonacci, 
    5:fizz_buzz
}

def asking_input():
    number = input("Input your selection : ")
    # Integer check
    script_executor(int(number)) if number.isdigit() else print("Please input only integer")

def script_executor(number):
    # Execute script if it exist
    script_list[number].continuation() if number in script_list else print("Can you see the number right there?")

def continuation():
    print('Logic Test by Farhan Sangaji. Select script to be executed first : ')
    for scr in script_list:
        print(f'{scr}. {script_list[scr].__name__}')
    asking_input()
    while True:
        if input('\nWant to execute another script?(Y/n)').lower() == "y":
            asking_input()
        else:
            print("Good Bye!")
            break

if __name__ == "__main__":
    continuation()


