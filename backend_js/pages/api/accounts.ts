// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { getAccounts } from './auth/[...nextauth]'

type Account = {
  email: string,
  image: string,
  name: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  getAccounts().then((data)=> {res.status(200).json(data)})
  
}
