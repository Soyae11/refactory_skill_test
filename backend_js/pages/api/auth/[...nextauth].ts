import NextAuth from "next-auth"
import GoogleProvider from "next-auth/providers/google"
import FacebookProvider from "next-auth/providers/facebook"
import { FirebaseAdapter, FirebaseClient } from "@next-auth/firebase-adapter"
import { getFirestore,runTransaction, collection, query, getDocs, where, limit, doc, getDoc, addDoc, updateDoc, deleteDoc } from "firebase/firestore";

import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAkjrpk3K2p9I3k2w8hkwrscaarkGXP6vg",
  authDomain: "refactory-skill-test-54093.firebaseapp.com",
  projectId: "refactory-skill-test-54093",
  storageBucket: "refactory-skill-test-54093.appspot.com",
  messagingSenderId: "251965410707",
  appId: "1:251965410707:web:d3f1972a50d01351a8ac66",
  measurementId: "G-XXR0REMFBT"
};
initializeApp(firebaseConfig)

const db = getFirestore();

export async function getAccounts() {
  const users = collection(db, 'users');
  const accounts = collection(db, 'accounts');
  const userSnapshot = await getDocs(users);
  const accountSnapshot = await getDocs(accounts);
  const userList = userSnapshot.docs.map(doc => {
    const accountList = accountSnapshot.docs.filter(acc => doc.id==acc.data().userId);
    const returner = doc.data()
    returner.additional = accountList[0].data()
    
    return returner
  });
  
  return userList;
}


const client = {
  db: db,
  collection,
  query,
  getDocs,
  where,
  limit,
  doc,
  getDoc,
  addDoc,
  updateDoc,
  deleteDoc,
  runTransaction
} as FirebaseClient

export default NextAuth({
    providers: [
      GoogleProvider({
        clientId: process.env.GOOGLE_ID!,
        clientSecret: process.env.GOOGLE_SECRET!,
      }),
      FacebookProvider({
        clientId: process.env.FACEBOOK_ID!,
        clientSecret: process.env.FACEBOOK_SECRET!
      }),
    ],
    adapter: FirebaseAdapter(client),
    secret: process.env.SECRET,
    session: {
        strategy:"jwt"
    },
    jwt: {
        secret: process.env.SECRET,
    },
    
    callbacks: {
        async signIn({ user, account, profile, email, credentials }) { return true },
        async redirect({ url, baseUrl }) { return baseUrl },
        async session({ session, token, user }) { return session },
        async jwt({ token, user, account, profile, isNewUser }) { return token }
    },
  });