# refactory_skill_test

## Logic Test

I am using Python 3.10, maybe the script will work in lower version, never tested it.

```
cd logic_test
python3 main.py 
```

## Json Manipulation

```
go to project's root directory
cd json_manipulation
python3 main.py 
```

## Mini App
[It's here] (https://refactory-skill-test.herokuapp.com/)

Diagram is in photos directory